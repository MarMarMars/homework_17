﻿#include <iostream>

class Example
{
private:
	int a, b, c;

public:

	Example() : a(1), b(2), c(3)
	{}

	Example(int newA, int newB, int newC)
	{
		a = newA;
		b = newB;
		c = newC;
	}

	int getA() {

		return a;
	}

	int getB() {

		return b;
	}

	int getC() {

		return c;
	}
	
};

class Vector
{
public:

	Vector() : x(0), y(0), z(0)
	{}

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show() {
		std::cout << x << " " << y << " " << z << '\n';
	}

	double Length() {

		return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
	}

private:
	double x, y, z;
};


int main()
{
	Example temp, temp1(5, 4, 3);
	Vector v, v1(5,8,9);

	v.Show();
	v1.Show();

	std::cout << v1.Length() << '\n';

	int sum = temp.getA() + temp.getB() + temp.getC();
	int sum2 = temp1.getA() + temp1.getB() + temp1.getC();

    std::cout << sum <<" "<<sum2;

}